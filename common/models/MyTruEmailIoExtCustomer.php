<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * MyTruEmailIoExtCustomer
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

class MyTruEmailIoExtCustomer extends MyTruEmailIoExtCommon
{
    public function getOptionKeyPrefix()
    {
        return 'customer_' . intval(Yii::app()->customer->getId()) . '.';
    }
}
