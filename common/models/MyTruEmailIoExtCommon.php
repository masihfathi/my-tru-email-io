<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * MyTruEmailIoExtCommon
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

class MyTruEmailIoExtCommon extends FormModel
{
    public $enabled = 'no';

    public $api_url = 'https://truemail.io/api/v1';

    public $api_key = '';

    public $customer_groups = array();

    public $check_zones = array();

    public function rules()
    {
        $rules = array(
            array('api_key, customer_groups, check_zones', 'safe'),
            array('api_url', 'url'),
            array('enabled', 'in', 'range' => array_keys($this->getYesNoOptions())),
        );
        return CMap::mergeArray($rules, parent::rules());
    }

    public function attributeLabels()
    {
        $extension = $this->getExtensionInstance();
        $labels = array(
            'enabled'          => Yii::t('app', 'Enabled'),
            'api_key'          => $extension->t('Api key'),
            'api_url'          => $extension->t('Api url'),
            'customer_groups'  => $extension->t('Customer groups'),
            'check_zones'      => $extension->t('Check zones'),
        );
        return CMap::mergeArray($labels, parent::attributeLabels());
    }

    public function attributePlaceholders()
    {
        $placeholders = array(
            'api_key' => '',
            'api_url' => 'https://truemail.io/api/v1',
        );
        return CMap::mergeArray($placeholders, parent::attributePlaceholders());
    }

    public function attributeHelpTexts()
    {
        $extension = $this->getExtensionInstance();
        $texts = array(
            'enabled'           => Yii::t('app', 'Whether the feature is enabled'),
            'api_key'           => $extension->t('The api key for the service'),
            'api_url'           => $extension->t('The api url for the service'),
            'customer_groups'   => $extension->t('Decide which customer groups can make use of this extension. If no group is selected, then all customers can use it. Please note that customers must use their own service credentials to validate the emails.'),
            'check_zones'       => $extension->t('Select the zones where email validation will run. If no zone is selected, then validation will run everywhere.'),

        );
        return CMap::mergeArray($texts, parent::attributeHelpTexts());
    }

    public function save()
    {
        $extension  = $this->getExtensionInstance();
        $attributes = array('enabled', 'api_key', 'api_url');
        foreach ($attributes as $name) {
            $extension->setOption($this->getOptionKeyPrefix() . $name, $this->$name);
        }
        $extension->setOption($this->getOptionKeyPrefix() . 'customer_groups', array_filter(array_map('intval', array_map('trim', (array)$this->customer_groups))));
        $extension->setOption($this->getOptionKeyPrefix() . 'check_zones', array_filter(array_map('trim', (array)$this->check_zones)));
        return $this;
    }

    public function populate()
    {
        $extension  = $this->getExtensionInstance();
        $attributes = array('enabled', 'api_key', 'api_url', 'customer_groups', 'check_zones');
        foreach ($attributes as $name) {
            $this->$name = $extension->getOption($this->getOptionKeyPrefix() . $name, $this->$name);
        }
        return $this;
    }

    public function getOptionKeyPrefix()
    {
        return '';
    }

    public function getCustomerGroupsList()
    {
        $groups = CustomerGroup::model()->findAll();
        $list   = array();

        foreach ($groups as $group) {
            $list[$group->group_id] = $group->name;
        }

        return $list;
    }

    public function getCheckZonesList()
    {
        $extension = $this->getExtensionInstance();
        $zones     = array();
        foreach (EmailBlacklist::getCheckZones() as $zone) {
            $zones[$zone] = $extension->t(ucwords(str_replace('_', ' ', $zone)));
        }
        return $zones;
    }

    public function getExtensionInstance()
    {
        return Yii::app()->extensionsManager->getExtensionInstance('my-tru-email-io');
    }
}
