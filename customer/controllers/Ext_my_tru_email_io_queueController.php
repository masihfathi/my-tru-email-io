<?php defined('MW_PATH') || exit('No direct script access allowed');

/** 
 * Controller file
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
 
class Ext_my_tru_email_io_queueController extends Controller
{
    // the extension instance
    public $extension;

	/**
	 * @param $list_uid
	 */
    public function actionAdd_list($list_uid)
    {
        $notify = Yii::app()->notify;        
        $list   = Lists::model()->findByAttributes(array(
        	'list_uid'    => $list_uid,
	        'customer_id' => (int)Yii::app()->customer->id,
        ));
        
        if (empty($list) || $list->getIsPendingDelete()) {
	        $notify->addError($this->extension->t('Invalid list!'));
	        return $this->redirect(array('lists/index'));
        }

	    $model = new MyTruEmailIoExtListsQueue();
	    $model->add($list->list_id);
        
	    $notify->addSuccess($this->extension->t('The list has been successfully added to queue!'));

        return $this->redirect(array('lists/index'));
    }

	/**
	 * @param $list_uid
	 */
	public function actionRemove_list($list_uid)
	{
		$notify = Yii::app()->notify;
		$list   = Lists::model()->findByAttributes(array(
			'list_uid'    => $list_uid,
			'customer_id' => (int)Yii::app()->customer->id,
		));

		if (empty($list)) {
			$notify->addError($this->extension->t('Invalid list!'));
			return $this->redirect(array('lists/index'));
		}
		
		$model = new MyTruEmailIoExtListsQueue();
		$model->remove($list->list_id);

		$notify->addSuccess($this->extension->t('The list has been successfully removed from queue!'));

		return $this->redirect(array('lists/index'));
	}
}