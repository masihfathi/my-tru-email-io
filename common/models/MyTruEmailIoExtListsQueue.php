<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * MyTruEmailIoExtListsQueue
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

class MyTruEmailIoExtListsQueue extends FormModel
{
	/**
	 * Flag for queue name
	 */
	const QUEUE_KEY = 'lists_queue';

	/**
	 * @var CMap
	 */
	private $_queue;
	
	/**
	 * @return mixed
	 */
    public function getExtensionInstance()
    {
        return Yii::app()->extensionsManager->getExtensionInstance('my-tru-email-io');
    }

	/**
	 * @return CMap
	 * @throws CException
	 */
    public function getQueue()
    {
    	if ($this->_queue === null) {
		    $this->_queue = new CMap((array)$this->getExtensionInstance()->getOption(self::QUEUE_KEY, array()));
	    }
	    return $this->_queue;
    }

	/**
	 * @param CMap $queue
	 *
	 * @return $this
	 */
    public function setQueue(CMap $queue)
    {
	    $this->getExtensionInstance()->setOption(self::QUEUE_KEY, $queue->toArray());
	    return $this;
    }

	/**
	 * @throws CException
	 */
    public function saveQueue()
    {
    	$this->setQueue($this->getQueue());
    	return $this;
    }

	/**
	 * @param $listId
	 *
	 * @return $this
	 * @throws CException
	 */
    public function add($listId)
    {
    	if (!$this->getQueue()->contains($listId)) {
    		$this->getQueue()->add($listId, array());
    		$this->saveQueue();
	    }
    	
	    return $this;
    }

	/**
	 * @param $listId
	 *
	 * @return $this
	 * @throws CException
	 */
    public function remove($listId)
    {
	    if ($this->getQueue()->contains($listId)) {
	    	$this->getQueue()->remove($listId);
	    	$this->saveQueue();
	    }
	    
	    return $this;
    }

	/**
	 * @param $listId
	 *
	 * @return bool
	 * @throws CException
	 */
    public static function isQueued($listId)
    {
	    $instance = new self();
	    return $instance->getQueue()->contains($listId);
    }
}
