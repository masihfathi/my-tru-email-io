<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * Controller file for TruEmail.io settings.
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

class Ext_my_tru_email_io_settingsController extends Controller
{
    // the extension instance
    public $extension;

    // move the view path
    public function getViewPath()
    {
        return Yii::getPathOfAlias('ext-my-tru-email-io.backend.views.settings');
    }

    /**
     * Common settings for TruEmail.io
     */
    public function actionIndex()
    {
        $request = Yii::app()->request;
        $notify  = Yii::app()->notify;

        $model = new MyTruEmailIoExtCommon();
        $model->populate();

        if ($request->isPostRequest) {
            $model->attributes = (array)$request->getPost($model->modelName, array());
            if ($model->validate()) {
                $notify->addSuccess(Yii::t('app', 'Your form has been successfully saved!'));
                $model->save();
            } else {
                $notify->addError(Yii::t('app', 'Your form has a few errors, please fix them and try again!'));
            }
        }

        $this->setData(array(
            'pageMetaTitle'    => $this->data->pageMetaTitle . ' | '. Yii::t('ext_my_tru_email_io', 'TruEmail.io'),
            'pageHeading'      => Yii::t('ext_my_tru_email_io', 'TruEmail.io'),
            'pageBreadcrumbs'  => array(
                Yii::t('app', 'Extensions') => $this->createUrl('extensions/index'),
                Yii::t('ext_my_tru_email_io', 'TruEmail.io') => $this->createUrl('ext_my_tru_email_io_settings/index'),
            )
        ));

        $this->render('index', compact('model'));
    }
}
