<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * TruEmailIoExt
 *
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */

class MyTruEmailIoExt extends ExtensionInit
{
    // name of the extension as shown in the backend panel
    public $name = 'TruEmail';

    // description of the extension as shown in backend panel
    public $description = 'Check email address validity using TruEmail service.';

    // current version of this extension
    public $version = '1.0';

    // minimum app version
    public $minAppVersion = '1.3.7.3';

    // the author name
    public $author = 'AvangEmail';

    // author website
    public $website = 'https://www.avangemail.com/';

    // contact email address
    public $email = 'support@avangemail.com';

    // in which apps this extension is allowed to run
    public $allowedApps = array('*');

    // cli enabled
    public $cliEnabled = true;

    // can this extension be deleted? this only applies to core extensions.
    protected $_canBeDeleted = true;

    // can this extension be disabled? this only applies to core extensions.
    protected $_canBeDisabled = true;

    // run the extension
    public function run()
    {
        Yii::import('ext-my-tru-email-io.common.models.*');
        if ($this->isAppName('backend')) {
            // add the url rules
            Yii::app()->urlManager->addRules(array(
                array('ext_my_tru_email_io_settings/index', 'pattern' => 'extensions/my-tru-email-io/settings'),
                array('ext_my_tru_email_io_settings/<action>', 'pattern' => 'extensions/my-tru-email-io/settings/*'),
            ));

            // add the controller
            Yii::app()->controllerMap['ext_my_tru_email_io_settings'] = array(
                'class'     => 'ext-my-tru-email-io.backend.controllers.Ext_my_tru_email_io_settingsController',
                'extension' => $this,
            );
        }
        
        // global collection
        if (!isset(Yii::app()->params['extensions.email-checkers.emails'])) {
            Yii::app()->params['extensions.email-checkers.emails'] = new CMap();
        }
        
        if ($this->getOption('enabled') != 'yes') {
            return;
        }

        if ($this->isAppName('customer')) {

            // register the url rule to resolve the pages.
            Yii::app()->urlManager->addRules(array(
                array('ext_my_tru_email_io_settings/index', 'pattern'    => 'settings/my-tru-email-io-settings'),
                array('ext_my_tru_email_io_settings/<action>', 'pattern' => 'settings/my-tru-email-io-settings/*'),
            ));

            // add the controllers
            Yii::app()->controllerMap['ext_my_tru_email_io_settings'] = array(
                'class'     => 'ext-my-tru-email-io.customer.controllers.Ext_my_tru_email_io_settingsController',
                'extension' => $this,
            );

            Yii::app()->hooks->addFilter('customer_left_navigation_menu_items', array($this, '_customerLeftNavigationMenuItems'));

        }

        Yii::app()->hooks->addFilter('email_blacklist_is_email_blacklisted', array($this, '_emailBlacklistIsEmailBlacklisted'));
    }

    // Add the landing page for this extension (settings/general info/etc)
    public function getPageUrl()
    {
        return Yii::app()->createUrl('ext_my_tru_email_io_settings/index');
    }

    // callback
    public function _emailBlacklistIsEmailBlacklisted($isBlacklisted, $email, ListSubscriber $subscriber = null, Customer $customer = null, array $params = array())
    {
        // if already blacklisted we stop
        if ($isBlacklisted !== false) {
            return $isBlacklisted;
        }

        // without customer we stop
        if (empty($customer)) {
            return $isBlacklisted;
        }
        $extension = Yii::app()->extensionsManager->getExtensionInstance('my-tru-email-io');
        $checkZone = !empty($params['checkZone']) ? $params['checkZone'] : '';
        $enabled   = (bool)($extension->getOption(sprintf('customer_%d.enabled', $customer->customer_id), 'no') == 'yes');
        $apiKey    = (string)$extension->getOption(sprintf('customer_%d.api_key', $customer->customer_id), '');
        $apiUrl    = (string)$extension->getOption(sprintf('customer_%d.api_url', $customer->customer_id), '');
        /// not enabled, no api key/url
        if (empty($enabled) || empty($apiKey) || empty($apiUrl)) {
            return $isBlacklisted;
        }

        $emails = Yii::app()->params['extensions.email-checkers.emails'];
        if ($emails->contains($email)) {
            return $emails->itemAt($email);
        }
        $emails->add($email, false);

        // check if the customer is allowed
        $allowedGroups = (array)$extension->getOption('customer_groups', array());
        $allowedGroups = !empty($allowedGroups) ? array_filter($allowedGroups) : array();
        if (!empty($allowedGroups) && !in_array($customer->group_id, $allowedGroups)) {
            return $emails->itemAt($email);
        }

        // check if the zone is allowed
        $checkZones = (array)$extension->getOption(sprintf('customer_%d.check_zones', $customer->customer_id), array());
        $checkZones = !empty($checkZones) ? array_filter($checkZones) : array();
        if (!empty($checkZones) && !in_array($checkZone, $checkZones)) {
            return $emails->itemAt($email);
        }

        $url    = sprintf('%s/verify/single?access_token=%s&email=%s', $apiUrl, $apiKey, $email);
        $result = AppInitHelper::simpleCurlGet($url);
        if (isset($result['status']) && $result['status'] != 'success') {
            return $emails->itemAt($email);
        }

        $result = json_decode($result['message']);
        if (empty($result) || !is_object($result) || empty($result->result)) {
            return $emails->itemAt($email);
        }

        if ($result->result == 'invalid') {
            $emails->add($email, (string)$result->result);
            return $emails->itemAt($email);
        }
        if(isset($result->error->statusCode) && preg_match('/^4/i',$result->error->statusCode)){
            $extension->setOption(sprintf('customer_%d.enabled', $customer->customer_id), 'no');
            return $emails->itemAt($email);
        }
        
        return $emails->itemAt($email);
    }

    // callback
    public function _customerLeftNavigationMenuItems($menuItems)
    {
        if (!isset($menuItems['settings'])) {
            return $menuItems;
        }

        if (is_string($menuItems['settings']['active'])) {
            $menuItems['settings']['active'] = array($menuItems['settings']['active']);
        }
        $menuItems['settings']['active'][] = 'ext_my_tru_email_io_settings';

        $extension = Yii::app()->extensionsManager->getExtensionInstance('my-tru-email-io');
        $route     = Yii::app()->getController()->getRoute();
        $menuItems['settings']['items'][] = array(
            'url'    => array('ext_my_tru_email_io_settings/index'),
            'label'  => $extension->t('TruEmail.io'),
            'active' => strpos($route, 'ext_my_tru_email_io_settings') === 0
        );
        return $menuItems;
    }
}
