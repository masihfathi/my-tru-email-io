<?php defined('MW_PATH') || exit('No direct script access allowed');

/**
 * @package MailWizz EMA
 * @subpackage TruEmail.io
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
 
?>
<ul class="nav nav-tabs" style="border-bottom: 0px;">
    <li class="<?php echo $this->getAction()->getId() == 'index' ? 'active' : 'inactive';?>">
        <a href="<?php echo $this->createUrl('ext_my_tru_email_io_settings/index')?>">
            <?php echo Yii::t('app', 'Common');?>
        </a>
    </li>
</ul>